package com.example.composedelegation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.composedelegation.composables.FirstComposable
import com.example.composedelegation.composables.SecondComposable
import com.example.composedelegation.composables.ThirdComposable
import com.example.composedelegation.ui.theme.ComposeDelegationTheme

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeDelegationTheme {
                val background = MaterialTheme.colorScheme.background
                var surfaceColor by remember { mutableStateOf(background) }
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = surfaceColor
                ) {
                    val navController = rememberNavController()
                    val resetAndNavigate: (String) -> Unit = {
                        surfaceColor = background
                        navController.navigate(it)
                    }
                    NavHost(navController = navController, startDestination = "first") {
                        composable(FIRST) {
                            FirstComposable(surfaceColor, { resetAndNavigate(SECOND) }) { surfaceColor = Color(it) }
                        }
                        composable(SECOND) {
                            SecondComposable(surfaceColor, { resetAndNavigate(THIRD) }) { surfaceColor = Color(it) }
                        }
                        composable(THIRD) { ThirdComposable { resetAndNavigate(FIRST) } }
                    }
                }
            }
        }
    }

    companion object {
        const val FIRST = "first"
        const val SECOND = "second"
        const val THIRD = "third"
    }
}
