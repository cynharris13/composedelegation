package com.example.composedelegation.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.tooling.preview.Preview
import com.example.composedelegation.delegates.FunColorDelegate
import com.example.composedelegation.delegates.FunColorDelegateImpl
import com.example.composedelegation.ui.theme.ComposeDelegationTheme

@Composable
fun SecondComposable(bg: Color, onClick: () -> Unit, onVeryFun: (Int) -> Unit) {
    // initialize the delegate
    val funColor: FunColorDelegate = FunColorDelegateImpl()
    funColor.setDefault(bg.toArgb())

    // ButtonColor methods can't be called within remember, so initial values are defined here
    var buttonColors = ButtonDefaults.buttonColors()
    val initalContainerColor = buttonColors.containerColor(enabled = true).value
    val initialTextColor = buttonColors.contentColor(enabled = true).value

    var containerColor by remember { mutableStateOf(initalContainerColor) }
    var textColor by remember { mutableStateOf(initialTextColor) }

    buttonColors = ButtonDefaults.buttonColors(containerColor = containerColor, contentColor = textColor)

    Column() {
        Button(colors = buttonColors, onClick = { onClick() }) { Text(text = "Next") }
        Button(
            colors = buttonColors,
            onClick = {
                containerColor = Color(funColor.currentColor)
                textColor = Color(1 - containerColor.red, 1 - containerColor.green, 1 - containerColor.blue)
                funColor.updateColor()
                onVeryFun(funColor.currentColor)
            }
        ) { Text(text = "Make the screen go fun") }
    }
}

@Preview(showBackground = true)
@Composable
fun SecondPreview() {
    ComposeDelegationTheme {
        SecondComposable(Color.Magenta, {}, {})
    }
}
