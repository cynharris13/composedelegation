package com.example.composedelegation.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.tooling.preview.Preview
import com.example.composedelegation.delegates.FunColorDelegate
import com.example.composedelegation.delegates.FunColorDelegateImpl
import com.example.composedelegation.ui.theme.ComposeDelegationTheme

@Composable
fun FirstComposable(bg: Color, onClick: () -> Unit, onFun: (Int) -> Unit) {
    val funColor: FunColorDelegate = FunColorDelegateImpl()
    funColor.setDefault(bg.toArgb())
    Column() {
        Text(text = "Hello")
        Button(onClick = { onClick() }) { Text(text = "Next") }
        Button(
            onClick = {
                funColor.updateColor()
                onFun(funColor.currentColor)
            }
        ) { Text(text = "Make the screen go fun") }
    }
}

@Preview(showBackground = true)
@Composable
fun FirstPreview() {
    ComposeDelegationTheme {
        FirstComposable(Color.Green, {}, {})
    }
}
