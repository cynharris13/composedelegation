package com.example.composedelegation.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.example.composedelegation.ui.theme.ComposeDelegationTheme

@Composable
fun ThirdComposable(onClick: () -> Unit) {
    Column() {
        Text(text = "Hello!!")
        Button(onClick = { onClick() }) {
            Text(text = "Reset!!")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ThirdPreview() {
    ComposeDelegationTheme {
        ThirdComposable({})
    }
}
