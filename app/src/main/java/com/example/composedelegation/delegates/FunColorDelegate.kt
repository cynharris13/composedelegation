package com.example.composedelegation.delegates

/**
 * Fun color delegate.
 *
 * @constructor Create empty Fun color delegate
 */
interface FunColorDelegate {
    var currentColor: Int
    var defaultColor: Int

    /**
     * Set default color.
     *
     * @param color
     */
    fun setDefault(color: Int)

    /**
     * Update color.
     *
     */
    fun updateColor()

    /**
     * Reset.
     *
     */
    fun reset()
}
