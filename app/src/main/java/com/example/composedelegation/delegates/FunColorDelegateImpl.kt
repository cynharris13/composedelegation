package com.example.composedelegation.delegates

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import kotlin.random.Random

/**
 * Fun color delegate implementation.
 *
 * @constructor Create empty Fun color delegate impl
 */
class FunColorDelegateImpl : FunColorDelegate {
    override var defaultColor: Int = 0
    override var currentColor: Int = nextRandomColor()

    override fun setDefault(color: Int) {
        defaultColor = color
        reset()
    }

    override fun updateColor() {
        currentColor = nextRandomColor()
    }

    override fun reset() {
        currentColor = defaultColor
    }
}
const val MAX = 255

/**
 * Next random color.
 *
 * @return
 */
fun nextRandomColor(): Int = Color(
    Random.nextInt(0, MAX),
    Random.nextInt(0, MAX),
    Random.nextInt(0, MAX)
).toArgb()
